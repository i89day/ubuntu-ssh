##89day/ubuntu-ssh
```
user:root
password:password
```

1、创建一个叫做 ubuntu-ssh 文件夹，用于存放编译镜像过程中用到的文件。
```
    cd /
    mkdir ubuntu-ssh
    cd ubuntu-ssh
    touch start.sh
    touch Dockerfile
```
~~2、编辑sources.list文件~~

3、编辑start.sh文件

4、编辑Dockerfile文件

5、编译镜像
```
    sudo docker build -t 89day/ubuntu-ssh .
    #最后一个.使用当前目录的Dockerfile    
```
6、创建容器，测试镜像功能
```
    sudo docker run --name 89day_ubuntu_ssh -d -p 32800:22 89day/ubuntu-ssh
    #32800:22 作用是将容器端口22映射为服务器端口32800
```
7、增加CentOS专用Dockerfile（Dockerfile(CentOS)），使用时改名为Dockerfile。